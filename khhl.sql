CREATE DATABASE web_store;
USE web_store;
CREATE TABLE IF NOT EXISTS items (
 ID            int         AUTO_INCREMENT PRIMARY KEY,
 name          varchar(20) NOT NULL,
 price         decimal     NULL,
 weight        float       NOT NULL,
 creation_date datetime    NULL,
 color         varchar(10) NULL
);

CREATE TABLE IF NOT EXISTS clients (
 ID            int         AUTO_INCREMENT PRIMARY KEY,
 name          varchar(20) NOT NULL,
 surname       varchar(30) NOT NULL,
 email         varchar(50) NULL,
 discount_card  int        NULL,
 wish_list 	   int		   NULL,
 phone_number  varchar(11) NULL,
 money decimal default 0
);
CREATE TABLE IF NOT EXISTS discount_cards (
 ID            int         AUTO_INCREMENT PRIMARY KEY,
 name          varchar(20) NOT NULL,
 percent       int         NOT NULL,
 client_id     int         NOT NULL
);
ALTER TABLE discount_cards
ADD CONSTRAINT id_client_cards
FOREIGN KEY (client_id)
REFERENCES clients(ID);

ALTER TABLE clients
ADD CONSTRAINT card_discount_clients
FOREIGN KEY (discount_card)
REFERENCES discount_cards(ID) 
ON DELETE CASCADE ON UPDATE SET NULL
;
 CREATE TABLE IF NOT EXISTS wishes_list (
 ID 	   int 	AUTO_INCREMENT PRIMARY KEY,
 item_id   int  NOT NULL,
 client_id int 	NOT NULL,
  FOREIGN KEY (client_id) REFERENCES clients(ID)
 );
ALTER TABLE wishes_list
ADD CONSTRAINT id_item
FOREIGN KEY (item_id)
REFERENCES items(ID);

CREATE TABLE IF NOT EXISTS promotions(
ID       int      AUTO_INCREMENT PRIMARY KEY,
end_time datetime NULL,
item_id  int      NOT NULL,
sale     decimal
);

CREATE INDEX sale ON promotions (sale) USING BTREE;

ALTER TABLE promotions 
ADD CONSTRAINT id_item_promotions
FOREIGN KEY (item_id)
REFERENCES items(ID);

CREATE TABLE IF NOT EXISTS basket (
ID int AUTO_INCREMENT PRIMARY KEY,
item_id int NOT NULL,
ammount int DEFAULT 1,
cost decimal NOT NULL,
promotion_sale int NOT NULL,
client_id int NOT NULL
); 

ALTER TABLE basket 
ADD CONSTRAINT id_item_basket
FOREIGN KEY (item_id)
REFERENCES items(ID);

ALTER TABLE basket
ADD CONSTRAINT id_client_basket
FOREIGN KEY (client_id)
REFERENCES clients(ID);

ALTER TABLE basket 
ADD CONSTRAINT sale_promotion_basket
FOREIGN KEY (promotion_sale)
REFERENCES promotions(ID);

CREATE TABLE IF NOT EXISTS orders (
ID int AUTO_INCREMENT PRIMARY KEY,
basket_id int NOT NULL,
price decimal NOT NULL,
client_id int NOT NULL
);

ALTER TABLE orders
ADD CONSTRAINT id_basket_orders
FOREIGN KEY (basket_id)
REFERENCES basket(ID); 

ALTER TABLE orders
ADD CONSTRAINT id_client_orders
FOREIGN KEY (client_id)
REFERENCES clients(ID);

DELIMITER $$
CREATE TRIGGER valid_sum BEFORE INSERT ON orders
FOR EACH ROW BEGIN
  IF price > clients_id.money THEN
SIGNAL SQLSTATE '02000' SET MESSAGE_TEXT = 'not enough money';
  END IF;
END$$
DELIMITER ;

CREATE TABLE IF NOT EXISTS ship (
ID int AUTO_INCREMENT PRIMARY KEY,
order_id int NOT NULL,
ship_cost decimal DEFAULT 50,
from_adress varchar (50) DEFAULT "Lviv",
to_adress varchar (50) NOT NULL,
post_service enum ("Ukrposta","Novaposta") DEFAULT "Novaposta"
);
ALTER TABLE ship 
ADD CONSTRAINT order_id_ship
FOREIGN KEY (order_id)
REFERENCES orders(ID);
